use std::env;
use std::fs;
use std::net::{TcpListener, TcpStream};
use std::io::*;
use serde::{Serialize, Deserialize};
use flate2::write::ZlibDecoder;


fn main() {
    let args = check_stdin();
    const addr:&str = "localhost:27182";
    let mut stream = TcpStream::connect(&addr).expect("Couldn't open Stream");
    let mut count: usize = 0;
    send_packet(&mut stream, &args);
    loop {
        if count <= args[3].parse::<usize>().unwrap() - args[2].parse::<usize>().unwrap() {
            recieve_rendered_files(&mut stream);
            count += 1;
            println!("This has ran {}", &count);
        }
        else {
            break;
        }
    }

    let done = Packet {
        id: Role::Customer,
        start_frame: 0,
        end_frame: 0,
        file: Vec::new(),
        file_name: String::from("Done"),
        file_size: 0
    };
    send_stuff(&done, &mut stream);

}

#[derive(Serialize, Deserialize, Debug)]
enum Role{
    Slave,
    Customer,
    Messenger
}

#[derive(Serialize, Deserialize, Debug)]
struct Packet {
    id: Role,
    start_frame: u32,
    end_frame: u32,
    file: Vec<u8>,
    file_name: String,
    file_size: u64

}

#[derive(Deserialize, Debug)]
struct RenderedPng {
    file: Vec<u8>,
    file_name: String
}

fn recieve_rendered_files(stream: &mut TcpStream){
    if let Err{..} = fs::create_dir("renders"){};

        let mut buff:[u8;8] = [0;8];
        let mut new_buff: Vec<u8>;
            match stream.read_exact(&mut buff){
                Ok(_) => {
                    match bincode::deserialize(&mut buff) {
                        Ok(size) => {
                            let size:usize = size;
                            println!("Size of transmission is {}", &size);
                            new_buff = vec![0; size];
                        }
                        Err(e) => {
                            panic!(e);
                        }
                    }
                }
                Err(e) => {
                    panic!("Some shit went wrong {:?}", e);
                }
            }
        if let Ok(r) = stream.read_exact(&mut new_buff[..]){
            println!("{:?}", &r);
        }
                    match bincode::deserialize(&new_buff) {
                        Ok(file) => {
                            let file: Packet = file;
                            println!("{}", &format!("renders/{}", file.file_name));
                            let destination = fs::File::create(
                            &format!("renders/{}", file.file_name)).unwrap();
                            let mut d = ZlibDecoder::new(destination);
                            d.write_all(&file.file[..]).unwrap();
                            d.finish().unwrap();
                        }
                        Err(e) => {
                            println!("{:?}", e);
                            panic!();
                        }
                    }
}

fn check_stdin() -> Vec<String> {
    let args: Vec<_> = env::args().collect();
    if args.len() > 3 {
        fs::File::open(&args[1]).expect("File does not exist"); 
        args[2].parse::<u32>().expect("Starting Frame Parse Error");
        args[3].parse::<u32>().expect("Ending Frame Parse Error");
    }
    else {
        panic!("Not enough Args");
    }
    return args;

}


fn send_packet(stream: &mut TcpStream, args: &Vec<String>)  {
    let mut file = fs::File::open(&args[1]).unwrap();
    let mut buff = Vec::with_capacity(file.metadata().expect("Couldn't get metadata").len() as usize);
    file.read_to_end(&mut buff).expect("Couldn't the contents of the file to the buffer");
    let file = Packet {
        id: Role::Customer,
        file_name: args[1].to_owned(),
        file_size: file.metadata().expect("Couldn't get metadata").len(),
        file: buff,
        start_frame: args[2].parse::<u32>().unwrap(),
        end_frame: args[3].parse::<u32>().unwrap()
    };
    send_stuff(&file, stream);
}


fn send_stuff<T>(buff: &T, stream: &mut TcpStream)
where T: serde::Serialize {
    let file = bincode::serialize(&buff).unwrap();
    let size = file.len();

    stream.write_all(&bincode::serialize(&size).unwrap()).unwrap();
    stream.write_all(&file).unwrap();
}
